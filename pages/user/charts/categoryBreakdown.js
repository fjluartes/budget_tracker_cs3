import { useState, useEffect } from 'react';
import { Form, Col } from 'react-bootstrap';
import { Pie } from 'react-chartjs-2';
import View from '../../../components/View';
import { API_URL, getAccessToken } from '../../../appHelper';
import { colorRandomizer } from '../../../colorRandomizer';
import moment from 'moment';

export default function CategoryBreakdown() {
  const [fromDate, setFromDate] = useState(`${moment().subtract(1, 'day').format("YYYY-MM-DD")}`);
  const [toDate, setToDate] = useState(`${moment().format("YYYY-MM-DD")}`);
  const [labelsArr, setLabelsArr] = useState([]);
  const [dataArr, setDataArr] = useState([]);
  const [bgColors,setBgColors] = useState([]);

  useEffect(() => {
    const accessToken = getAccessToken();
    getBreakdown(accessToken, fromDate, toDate);
  }, [fromDate, toDate]);

  const getBreakdown = (accessToken, fromDate, toDate) => {
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: JSON.stringify({
        fromDate,
        toDate
      })
    };
    fetch(`${API_URL}/users/get-records-breakdown`, options).then(res => {
      return res.json();
    }).then(data => {
      // console.log('records-breakdown', data);
      setLabelsArr(data.map(item => item.categoryName));
      setDataArr(data.map(item => item.totalAmount));
      setBgColors(data.map(() => `#${colorRandomizer()}`));
    });
  };

  const pieData = {
    labels: labelsArr,
    datasets: [{
      data: dataArr,
      backgroundColor: bgColors,
      hoverBackgroundColor: bgColors
    }]
  };

  return (
    <View title="Category Breakdown">
      <h3>Category Breakdown</h3>
      <Form.Row>
        <Form.Group as ={Col} xs="6">
          <Form.Label>From</Form.Label>
          <Form.Control
            type="date"
            value={fromDate}
            onChange={(e) => setFromDate(e.target.value)}
          />
        </Form.Group>
        <Form.Group as ={Col} xs="6">
          <Form.Label>To</Form.Label>
          <Form.Control
            type="date"
            value={toDate}
            onChange={(e) => setToDate(e.target.value)}
          />
        </Form.Group>
      </Form.Row>
      <Pie data={pieData} />
    </View>
  );
}
