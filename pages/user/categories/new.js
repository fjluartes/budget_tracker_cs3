import { useState, useEffect } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import Head from 'next/head';
import View from '../../../components/View';
import { API_URL, getAccessToken } from '../../../appHelper';
import Swal from 'sweetalert2';

export default function AddCategory() {  
  return (
    <View title={'Add Category'}>
      <Row className="justify-content-center">
        <Col md={8}>
          <h3 className="my-5 bg-dark text-white text-center">New Category</h3>
          <Card>
            <Card.Header>Category Information</Card.Header>
            <Card.Body>
              <NewCategoryForm/>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </View>
  );
}

const NewCategoryForm = () => {
  const [name, setName] = useState('');
  const [type, setType] = useState('');

  const createCategory = (e) => {
    e.preventDefault();
    const accessToken = getAccessToken();
    let payload = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: JSON.stringify({
        name,
        type
      })
    };
    fetch(`${API_URL}/users/add-category`, payload).then(res => {
      return res.json();
    }).then(data => {
      // console.log('add category', data);
      if (data) {
        Swal.fire({
          icon: "success",
          title: "Category Added",
          text: "Category Added Successfully",
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Failed",
          text: "Something went wrong"
        });
      }
      setName('');
      setType('');
    });
  };

  return (
    <Form onSubmit={(e) => createCategory(e)}>
      <Form.Group controlId="name">
        <Form.Label>Category Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Category Name"
          value={name}
          onChange={(e) => setName(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group controlId="type">
        <Form.Label>Category Type</Form.Label>
        <Form.Control
          as="select"
          value={type}
          onChange={(e) => setType(e.target.value)}
          required
        >
          <option></option>
          <option>Income</option>
          <option>Expense</option>
        </Form.Control>
      </Form.Group>
      <Button
        variant="warning"
        type="submit"
      >
        Submit
      </Button>
    </Form>
  );
};
