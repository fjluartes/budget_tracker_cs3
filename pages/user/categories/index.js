import { useState, useEffect } from 'react';
import { Form, Button, Row, Col, Table } from 'react-bootstrap';
import Head from 'next/head';
import Link from 'next/link';
import View from '../../../components/View';
import { API_URL, getAccessToken } from '../../../appHelper';

export default function Categories() {
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    const accessToken = getAccessToken();
    getCategories(accessToken);
  }, []);

  const getCategories = (accessToken) => {
    const options = {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${accessToken}`
      }
    };
    fetch(`${API_URL}/users/get-categories`, options).then(res => {
      return res.json();
    }).then(data => {
      // console.log('categories', data);
      setCategories(data);
    });
  };

  const categoriesList = categories.map((category, i) => {
    return (
      <tr key={i}>
        <td>{category.name}</td>
        <td>{category.type}</td>
      </tr>
    );
  });
  
  return (
    <View title={'Categories'}>
      <Row className="justify-content-center">
        <Col md={8}>
          <h1 className="my-5 bg-dark text-white text-center">Categories</h1>
          <Link href="/user/categories/new">
            <Button
              variant="warning"
              type="submit"
              className="mt-1 mb-3"
            >
              Add
            </Button>
          </Link>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Category</th>
                <th>Type</th>
              </tr>
            </thead>
            <tbody>
              {categoriesList}
            </tbody>
          </Table>
        </Col>
      </Row>
    </View>
  );
}
