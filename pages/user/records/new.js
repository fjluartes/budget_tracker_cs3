import { useState, useEffect } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import Head from 'next/head';
import View from '../../../components/View';
import { API_URL, getAccessToken } from '../../../appHelper';
import Swal from 'sweetalert2';

export default function NewRecord() {
  return (
    <View title={'Add Record'}>
      <Row className="justify-content-center">
        <Col md={8}>
          <h3 className="my-5 bg-dark text-white text-center">New Record</h3>
          <Card>
            <Card.Header>Record Information</Card.Header>
            <Card.Body>
              <NewRecordForm/>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </View>
  );
}

const NewRecordForm = () => {
  const [categoryName, setCategoryName] = useState('');
  const [type, setType] = useState('');
  const [amount, setAmount] = useState(0);
  const [description, setDescription] = useState('');
  const [namesArr, setNamesArr] = useState([]);

  useEffect(() => {
    getCategories(type);
  }, [type]);

  const getCategories = (type) => {
    const accessToken = getAccessToken();
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: JSON.stringify({
        type
      })
    };
    fetch(`${API_URL}/users/get-categories`, options).then(res => {
      return res.json();
    }).then(data => {
      // console.log('categories', data);
      setNamesArr(data);
    });
  };

  const namesList = namesArr.map((category, i) => {
    return (
      <option key={i}>{category.name}</option>
    );
  });

  const createRecord = (e) => {
    e.preventDefault();
    const accessToken = getAccessToken();
    let payload = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: JSON.stringify({
        categoryName,
        type,
        amount,
        description
      })
    };
    fetch(`${API_URL}/users/add-record`, payload).then(res => {
      return res.json();
    }).then(data => {
      // console.log('add category', data);
      if (data) {
        Swal.fire({
          icon: "success",
          title: "Record Added",
          text: "Record Added Successfully",
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Failed",
          text: "Something went wrong"
        });
      }
      setCategoryName('');
      setType('');
      setAmount(0);
      setDescription('');
    });
  };
  
  return (
    <Form onSubmit={(e) => createRecord(e)}>
      <Form.Group controlId="type">
        <Form.Label>Category Type</Form.Label>
        <Form.Control
          as="select"
          value={type}
          onChange={(e) => setType(e.target.value)}
          required
        >
          <option></option>
          <option>Income</option>
          <option>Expense</option>
        </Form.Control>
      </Form.Group>
      <Form.Group controlId="categoryName">
        <Form.Label>Category Name</Form.Label>
        <Form.Control
          as="select"
          value={categoryName}
          onChange={(e) => setCategoryName(e.target.value)}
          required
        >
          <option></option>
          {namesList}
        </Form.Control>
      </Form.Group>
      <Form.Group controlId="amount">
        <Form.Label>Amount</Form.Label>
        <Form.Control
          type="number"
          placeholder="Enter Amount"
          value={amount}
          onChange={(e) => setAmount(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group controlId="description">
        <Form.Label>Description</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Description"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
          required
        />
      </Form.Group>
      <Button
        variant="warning"
        type="submit"
      >
        Submit
      </Button>
    </Form>
  );
};
