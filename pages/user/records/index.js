import { useState, useEffect, useContext, Fragment } from 'react';
import { Form, Button, Row, Col, Card, InputGroup } from 'react-bootstrap';
import Head from 'next/head';
import Link from 'next/link';
import View from '../../../components/View';
import { API_URL, getAccessToken } from '../../../appHelper';
import moment from 'moment';

export default function Records() {
  const [searchKeyword, setSearchKeyword] = useState('');
  const [searchType, setSearchType] = useState("All");

  return (
    <View title={'Records'}>
      <Row className="justify-content-center">
        <Col md={8}>
          <h1 className="my-5 bg-dark text-white text-center">Records</h1>
          <InputGroup className="mb-2">
            <InputGroup.Prepend>
              <Link href="/user/records/new">
                <a className="btn btn-warning" type="submit">Add</a>
              </Link>
            </InputGroup.Prepend>
            <Form.Control
              placeholder="Search Record"
              value={searchKeyword}
              onChange={(e) => setSearchKeyword(e.target.value)}
            />
            <Form.Control
              as="select"
              defaultValue={searchType}
              onChange={(e) => setSearchType(e.target.value)}
            >
              <option value="All">All</option>
              <option value="Income">Income</option>
              <option value="Expense">Expense</option>
            </Form.Control>
          </InputGroup>
          <RecordsView
            searchKeyword={searchKeyword}
            searchType={searchType}
          />
        </Col>
      </Row>
    </View>
  );
}

const RecordsView = ({ searchKeyword, searchType }) => {
  const [records, setRecords] = useState([]);

  useEffect(() => {
    const accessToken = getAccessToken();
    getRecords(accessToken, searchKeyword, searchType);
  }, [searchKeyword, searchType]);

  const getRecords = (accessToken, searchKeyword, searchType) => {
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      }
    };
    if (searchType !== 'All') {
      options.body = JSON.stringify({
        type: searchType,
        description: searchKeyword
      });
    } else {
      options.body = JSON.stringify({
        description: searchKeyword
      });
    }
    fetch(`${API_URL}/users/get-records`, options).then(res => {
      return res.json();
    }).then(data => {
      // console.log('records', data);
      setRecords(data);
    });
  };
  
  return (
    <Fragment>
      {records.length > 0 ?
        records.map((record) => {
          const textColor = (record.type === 'Income') ? 'text-success' : 'text-danger';
          const amountSymbol = (record.type === 'Income') ? '+' : '-';
          
          return (
            <Card className="mb-3" key={record._id}>
              <Card.Body>
                <Row>
                  <Col xs={6}>
                    <h5>{record.description}</h5>
                    <h6><span className={textColor}>{record.type}</span>{ ' (' + record.categoryName + ')' }</h6>
                    <p>{moment(record.dateAdded).format("MMMM D, YYYY")}</p>
                  </Col>
                  <Col xs={6} className="text-right">
                    <h6 className={textColor}>{ amountSymbol + ' ' + record.amount.toLocaleString() }</h6>
                    <span className={textColor}>{record.balanceAfterTransaction.toLocaleString()}</span>
                  </Col>
                </Row>
              </Card.Body>
            </Card>
          );
        })
       :
       <div></div>
      }
    </Fragment>
  );
};
