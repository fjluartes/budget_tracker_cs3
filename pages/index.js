import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import Head from 'next/head';
import Link from 'next/link';
import Router from 'next/router';
import View from '../components/View';
import { API_URL } from '../appHelper';
import Swal from 'sweetalert2';
import UserContext from '../contexts/UserContext';
import GoogleLoginButton from '../components/GoogleLoginButton';

export default function Home() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [inactive, setInactive] = useState(true);
  const { user, setUser } = useContext(UserContext);

  useEffect(() => {
    if (email !== "" && password !== "") {
      setInactive(false);
    } else {
      setInactive(true);
    }
  }, [email, password]);

  const getUserDetails = (accessToken) => {
    const options = {
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    };
    fetch(`${API_URL}/users/details`, options).then(res => {
      return res.json();
    }).then(data => {
      // console.log('details', data);
      setUser({ email: data.email });
      Router.push('/user/records');
    });
  };

  const loginUser = (e) => {
    e.preventDefault();
    let userObj = {
      email,
      password
    };
    let payload = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(userObj)
    };
    fetch(`${API_URL}/users/login`, payload).then(res => {
      return res.json();
    }).then(data => {
      // console.log('token', data);
      if (typeof data.accessToken !== 'undefined') {
        localStorage.setItem('token', data.accessToken);
        getUserDetails(data.accessToken);
        Swal.fire({
          icon: "success",
          title: "Success",
          text: "Login Successful",
          confirmButtonText: 'OK',
        }).then(() => {
          Router.push('/user/records');
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Login Failed",
          text: "Something went wrong"
        });
      }
      setEmail("");
      setPassword("");
    });
  };
  
  return (
    <View title={'CoinGuard'}>
      <Row className="justify-content-center">
        <Col md={8}>
          <h1 className="my-5 bg-dark text-white text-center">Welcome!</h1>
          <p>Login by using your Registered Email</p>
          <Form onSubmit={(e) => loginUser(e)}>
            <Form.Group controlId="email">
              <Form.Label>Email:</Form.Label>
              <Form.Control
                type="text"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group controlId="password">
              <Form.Label>Password:</Form.Label>
              <Form.Control
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </Form.Group>
            <Button
              variant="warning"
              type="submit"
              className="btn-block mb-3"
              disabled={inactive}
            >
              Login
            </Button>
          </Form>
          <span>-- or --</span>
          <GoogleLoginButton page={'Login'}/>
          <p>Don't have an account? <Link href="/register">Register Here</Link></p> 
        </Col>
      </Row>
    </View>
  );
}
