import { useState, useEffect } from 'react';
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';
import AppNavbar from '../components/NavBar';
import { UserProvider } from '../contexts/UserContext';
import { API_URL_LOCAL, getAccessToken } from '../appHelper';

function MyApp({ Component, pageProps }) {
  const [user, setUser] = useState({
    email: null,
    categories: [],
    transactions: []
  });

  useEffect(() => {
    if (getAccessToken() !== null) {
      const options = {
        headers: {
          Authorization: `Bearer ${getAccessToken()}`
        }
      };
      fetch(`${API_URL_LOCAL}/users/details`, options).then(res => {
        return res.json();
      }).then(userData => {
        console.log(userData);
        if (typeof userData.email !== 'undefined') {
          setUser({ email: userData.email });
        } else {
          setUser({ email: null }); 
        }
      });
    }
  }, [user.id]);

  const unsetUser = () => {
    localStorage.clear();
    setUser({ email: null }); 
  };
  
  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <AppNavbar />
      <Container>
        <Component {...pageProps} />
      </Container>
    </UserProvider>
  );
}

export default MyApp;
