import { useState, useEffect } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import Head from 'next/head';
import Router from 'next/router';
import View from '../components/View';
import { API_URL } from '../appHelper';
import Swal from 'sweetalert2';
import GoogleLoginButton from '../components/GoogleLoginButton';

export default function Register() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState(0);
  const [password, setPassword] = useState('');
  const [verifyPassword, setVerifyPassword] = useState('');
  const [inactive, setInactive] = useState(true);

  useEffect(() => {
    let isFirst = firstName !== "";
    let isLast = lastName !== "";
    let isMobile = mobileNo !== "";
    let isEmail = email !== "";
    let isPass = password !== "";
    let isVerifyPass = verifyPassword !== "";
    let isEqual = password === verifyPassword;
    if (isFirst && isLast && isMobile && isEmail && isPass && isVerifyPass && isEqual) {
      setInactive(false);
    } else {
      setInactive(true);
    }
  }, [firstName, lastName, mobileNo, email, password, verifyPassword]);

  const registerUser = (e) => {
    e.preventDefault();
    fetch(`${API_URL}/users/email-exists`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email
      })
    }).then(res => {
      return res.json();
    }).then(data => {
      if (data) {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: "Email already exists"
        });
      } else {
        let registerObj = {
          firstName,
          lastName,
          email,
          mobileNo,
          password
        };
        let payload = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(registerObj)
        };
        fetch(`${API_URL}/users/register`, payload).then(res => {
          return res.json();
        }).then(data => {
          // console.log(data);
          if (data) {
            Swal.fire({
              icon: "success",
              title: "Success",
              text: "New User Registered Successfully",
              confirmButtonText: 'Proceed to Login',
            }).then(() => {
              Router.push('/');
            });
          } else {
            Swal.fire({
              icon: "error",
              title: "Registration Failed",
              text: "Something went wrong"
            });
          }
          setFirstName("");
          setLastName("");
          setMobileNo(0);
          setEmail("");
          setPassword("");
          setVerifyPassword("");
        });
      }
    });
  };
  
  return (
    <View title={'CoinGuard'}>
      <Row className="justify-content-center">
        <Col md={8}>
          <h1 className="my-5 bg-dark text-white text-center">Register</h1>
          <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group controlId="firstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter First Name"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group controlId="lastName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Last Name"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group controlId="email">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group controlId="mobileNo">
              <Form.Label>Mobile Number</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter Mobile Number"
                value={mobileNo}
                onChange={(e) => setMobileNo(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Enter Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group controlId="verifyPassword">
              <Form.Label>Verify Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Verify your Password"
                value={verifyPassword}
                onChange={(e) => setVerifyPassword(e.target.value)}
                required
              />
            </Form.Group>
            <Button
              variant="warning"
              type="submit"
              disabled={inactive}
            >
              Register
            </Button>
          </Form>
          <span>-- or --</span>
          <GoogleLoginButton page={'Register'}/>
        </Col>
      </Row>
    </View>
  );
}
