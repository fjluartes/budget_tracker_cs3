import { useContext } from 'react';
import { API_URL } from '../appHelper';
import { GoogleLogin } from 'react-google-login';
import GoogleButton from 'react-google-button';
import Router from 'next/router';
import Swal from 'sweetalert2';
import UserContext from '../contexts/UserContext';

export default function GoogleLoginButton({ page }) {
  const { setUser } = useContext(UserContext);
  let label = `${page} with Google`;
  const clientId = "1033889835380-ir9i216auqj055hc57thlnunbh088791.apps.googleusercontent.com";
  const getUserDetails = (accessToken) => {
    const options = {
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    };
    fetch(`${API_URL}/users/details`, options).then(res => {
      return res.json();
    }).then(data => {
      // console.log('details', data);
      setUser({ email: data.email });
      Router.push('/user/records');
    });
  };
  const getGoogleDetails = (response) => {
    const payload = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        tokenId: response.tokenId
      })
    };
    fetch(`${API_URL}/users/verify-google-id-token`, payload).then(res => {
      return res.json();
    }).then(data => {
      // console.log(data);
      localStorage.setItem('token', data.accessToken);
      getUserDetails(data.accessToken);
      if (data) {
        Swal.fire({
          icon: "success",
          title: "Success",
          text: "Login Successful",
          confirmButtonText: 'OK',
        });
      } else {
        Swal.fire({
          icon: "error",
          title: "Login Failed",
          text: "Something went wrong"
        });
      }
    });
  };

  return (
    <GoogleLogin
      clientId={clientId}
      render={renderProps => (
        <GoogleButton onClick={renderProps.onClick} disabled={renderProps.disabled} label={label} />
      )}
      onSuccess={getGoogleDetails}
      onFailure={getGoogleDetails}
      cookiePolicy={'single_host_origin'}
    />
  );
};
