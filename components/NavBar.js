import { Fragment, useState, useEffect, useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import Link from 'next/link';
import UserContext from '../contexts/UserContext';

export default function NavBar() {
  const { user } = useContext(UserContext);
  const  [isExpanded, setIsExpanded] = useState(false);

  let RightNavOptions;
  let LeftNavOptions;

  if (user.email !== null) {
    LeftNavOptions = (
      <Fragment>
        <Link href="/user/categories">
          <a href="#" className="nav-link">Categories</a>
        </Link>
        <Link href="/user/records">
          <a href="#" className="nav-link">Records</a>
        </Link>
        <Link href="/user/charts/categoryBreakdown">
          <a href="#" className="nav-link">Breakdown</a>
        </Link>
      </Fragment>
    );
    RightNavOptions = (
      <Fragment>
        <Link href="/logout">
          <a href="#" className="nav-link">Logout</a>
        </Link>
      </Fragment>
    );
  } else {
    LeftNavOptions = null;
    RightNavOptions = null;
  }

  return (
    <Fragment>
      <Navbar bg="dark" variant="dark" expand="lg" expanded={isExpanded}>
        <Link href="/">
          <a href="#" className="navbar-brand">CoinGuard</a>
        </Link>
        <Navbar.Toggle aria-controls="basic-navbar-nav" onClick={() => setIsExpanded(!isExpanded)} />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto" onClick={() => setIsExpanded(!isExpanded)}>
            {LeftNavOptions}
          </Nav>
          <Nav className="ml-auto" onClick={() => setIsExpanded(!isExpanded)}>
            {RightNavOptions}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </Fragment>
  );
};
