module.exports = {
  API_URL: process.env.NEXT_PUBLIC_API_URL_KEY,
  API_URL_LOCAL: process.env.NEXT_PUBLIC_API_URL_KEY_LOCAL,
  getAccessToken: () => localStorage.getItem('token')
};
